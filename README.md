# arch-repro-tools

A collection of small tools to make it easier to work on reproducible builds.

## reproBuildPatch
This file takes the `${pkgname}-${pkgver}` source folder and diffs it to the `${pkgname}-${pkgver}_orig` folder. The resulting diff is stored in `${pkgname}-reproducible.patch` file.
If the package provides multiple pkgnames (like [php](https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/php)), the first entry of the `pkgname` array is used as pkgname.
At the end `reproBuildPatch` also runs `updpkgsums`, so once you added it to the `PKGBUILD` file with the current hashsum it will automatically update the `sha256sum` of the `source` entry.

## reproDiff
Compares all `*.pkg.tar.zst` packages in your build directory with the corresponding repro build under `/var/lib/archbuild/reproducible/testenv/pkgdest` using `diffoscope`.
